
**org.bitbucket.tbrugz** maven repository
=========================================

This is a public artifact repository of *org.bitbucket.tbrugz* organization's projects
(see [https://bitbucket.org/tbrugz](https://bitbucket.org/tbrugz)).
It is a maven2-compatible repository and may be used with [maven](http://maven.apache.org/),
[ivy](http://ant.apache.org/ivy/), or another compatible dependency manager.

To use with **ivy**, use a resolver like the following:
```
#!XML
<ibiblio
	name="bitbucket-tbrugz"
	root="https://bitbucket.org/tbrugz/mvn-repo/raw/master/"
	m2compatible="true" />
```

A full ivysettings example is avaiable [on this mvn-repo](https://bitbucket.org/tbrugz/mvn-repo/src/tip/ivysettings.xml).

A more complete ivysettings example (with a local mvn publish resolver) may be seen at 
[queryon's ivysettings.xml](https://bitbucket.org/tbrugz/queryon/src/tip/ivysettings.xml).

To use with **maven**, you may add the following snippet to your `pom.xml`:
```
#!XML
<repositories>
    <repository>
        <id>org.bitbucket.tbrugz.mvn.repo</id>
        <name>org.bitbucket.tbrugz maven repository</name>
        <url>https://bitbucket.org/tbrugz/mvn-repo/raw/master/</url>
        <layout>default</layout>
    </repository>
</repositories>
```
